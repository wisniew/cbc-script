#!/bin/bash

#
# CBC-Script for CBC app
#


# Variables
VERSION=1
APPFOLDER="/sdcard/Android/data/com.wisniew.cherrybashcommander";
#GOOGLE-LINK ="https://drive.google.com/uc?export=download&id=" #TODO google download link template

# inputs list:
# 1 - onCreate process
# 2 - update app
# 3 - update kernel

INPUT=$1

if [ "$INPUT" == "1" ]; # onCreate process
then

     #if [ ! -e "$APPFOLDER/version_script_number.txt" ];
     #then
     #     touch $APPFOLDER/version_script_number.txt
     #fi
     #echo $VERSION > $APPFOLDER/version_script_number.txt
     
     if [ ! -e "$APPFOLDER/version_app_number.txt" ];
     then
          touch $APPFOLDER/version_app_number.txt
     fi
     echo $2 > $APPFOLDER/version_app_number.txt
     

     #curl -L -o $APPFOLDER/new/version_script_number.txt "https://drive.google.com/uc?export=download&id=1GNbGNY_nnYOGH5xok7BGNPmVH0w0pxpQ"

     #NEW-VERSION-SCRIPT=$(cat $APPFOLDER/new/version_script_number.txt)
     #VERSION-SCRIPT=$(cat $APPFOLDER/version_script_number.txt)

     #if [ "$NEW-VERSION-SCRIPT" > "$VERSION-SCRIPT" ];  # TODO do it in better way...
     #then
     #     echo "Updating script..."
     #     curl -L -o $APPFOLDER/new/cbc-script.sh https://drive.google.com/uc?export=download&id=1t1VdntQPCU2Mq9bEZeensFNifGXHm659
     #     mv $APPFOLDER/new/cbc-script.sh $APPFOLDER/cbc-script.sh
          #TODO checking if updated to newest version
     #     echo "Updating script done!"
     #fi
     #exit 0
fi

if [ "$INPUT" == "2" ]; # update app
then

     echo "Zbyszek! "
     rm -rf $APPFOLDER/new
     mkdir $APPFOLDER/new
     curl -L -o $APPFOLDER/new/version_app_number.txt "https://drive.google.com/uc?export=download&id=1B3b0eBIYD-sGI3zLQlN_zme5uB4WiOzn"

     NEW-VERSION-APP=$(cat $APPFOLDER/new/version_app_number.txt)
     VERSION-APP=$(cat $APPFOLDER/version_app_number.txt)

     if [ "$NEW-VERSION-APP" > "$VERSION-APP" ];
     then
          echo "Updating APP..."
          curl -L -o $APPFOLDER/cbc.apk "https://drive.google.com/uc?export=download&id=1_wdL3sD6jkLHzjun8r7NEZYM8y09tsAj"
          pm install -r $APPFOLDER/cbc.apk
          echo "Downloading app done!"
          fi
          #rm -rf $APPFOLDER/new;
     exit 0
fi

if [ "$INPUT" == "3" ]; # update kernel
then
     curl -L -o $APPFOLDER/update.zip #TODO file link
     echo "boot-recovery " > /cache/recovery/command
     echo "--update_package=$APPFOLDER/update.zip" >> /cache/recovery/command
     reboot recovery
fi


exit 0
